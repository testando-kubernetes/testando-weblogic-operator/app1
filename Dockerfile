# Copyright (c) 2021, Oracle and/or its affiliates.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

# This is a sample Dockerfile for supplying Model in Image model files
# and a WDT install in a small separate auxiliary image
# image. This is an alternative to supplying the files directly
# in the domain resource `domain.spec.image` image.

# AUXILIARY_IMAGE_PATH arg:
#   Parent location for Model in Image model and WDT installation files.
#   Must match domain resource 'domain.spec.auxiliaryImageVolumes.mountPath'
#   For model-in-image, the following two domain resource attributes can
#   be a directory in the mount path:
#     1) 'domain.spec.configuration.model.modelHome'
#     2) 'domain.spec.configuration.model.wdtInstallHome'
#   Default '/auxiliary'.
#

FROM maven:3.9.4-eclipse-temurin-17-alpine as java-build
ARG VERSION=1-SNAPSHOT
WORKDIR /app
COPY . /app
RUN mvn versions:set -DnewVersion=${VERSION}
RUN mvn clean package

FROM alpine:3.18.3 as archive-build
ARG ARTIFACT_ID
ARG VERSION=1-SNAPSHOT
RUN apk add --no-cache zip
WORKDIR /archive
COPY --from=java-build ./app/target/$ARTIFACT_ID-${VERSION}.war ./wlsdeploy/applications/$ARTIFACT_ID/$ARTIFACT_ID-${VERSION}.war
RUN zip -r archive.zip wlsdeploy

FROM alpine:3.18.3 as model-build
ARG ARTIFACT_ID
ARG VERSION=1-SNAPSHOT
ARG PACKAGING=war
ARG TYPE=Application
RUN apk add --no-cache gettext
WORKDIR /models
COPY ./models /templates
RUN envsubst < /templates/template.model.yaml >> $ARTIFACT_ID.model.yaml
RUN envsubst < /templates/template.model.properties >> $ARTIFACT_ID.model.properties

FROM busybox
ARG AUXILIARY_IMAGE_PATH=/auxiliary
ARG USER=oracle
ARG USERID=1000
ARG GROUP=root
ENV AUXILIARY_IMAGE_PATH=${AUXILIARY_IMAGE_PATH}
RUN adduser -D -u ${USERID} -G $GROUP $USER
COPY --from=model-build ./models ${AUXILIARY_IMAGE_PATH}/models
COPY --from=archive-build /archive/archive.zip ${AUXILIARY_IMAGE_PATH}/models
RUN chown -R $USER:$GROUP ${AUXILIARY_IMAGE_PATH}/
USER $USER
